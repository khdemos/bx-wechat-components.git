# list-view 组件

## properties

|  属性名称     | 默认值   |  必须   | 描述                 |
|  :--------:  | :-------:| :----:  | :-------------:     |
| datas        | []       |true     |列表数据          |
| error       | false   |true    |数据是否发生错误（网络异常）|
| useRefresh   | true     |false    |是否开启下拉刷新          |
| useLoadMore  | true      |false    |是否开启加载更多         |
| limit        | 10       |false    |分页： 每页条数           |
| throttle     | true     |false    |如果到底不触发加载更多，throttle设置为false, 开启不限制scrollview节流，尽量不要设置，可能有性能问题           |
| loading     | false     |false    |list-view 初始化正在加载状态组件，默认为false，如果page页面没有初始化加载loading，可以开启此属性，当列表数据初次加载完毕后，需要关闭此属性          |
| adapterIPhoneX     | true     |false    | 适配iphonex         |

## 事件

|  事件名称     | 描述                 |
|  :--------:  | :-------------:     |
| onRefresh    | 下拉刷新事件          |
| onLoadMore    | 加载更多事件          |

## 代码示例

```
Page({
  data: {
    loading: true,
    datas: [],
    banners: [],
    noMoreData: false,
    error: false,
  },
  onLoad() {
    // 模拟网络请求
    setTimeout(() => {
      this.setData({
        banners,
        datas,
        error: false,
        loading: false,
      })
    }, 1000)
  },
  onLoadMore(e) {
    let d = Object.assign([], this.data.datas)
    setTimeout(() => {
      if (this.data.datas.length > 10) {
        this.setData({
          error: true,
        })
      } else {
        this.setData({
          error: false,
          datas: [...d, ...datas1]
        })
      }
    }, 1000)
  },
  onRefresh(e) {
    setTimeout(() => {
      if (this.data.datas.length > 20) {
        this.setData({
          datas,
          error: false
        })
      } else {
        this.setData({
          datas,
          error: false
        })
      }
    }, 1000)
  }
})

```

```
<list-view loading="{{loading}}" datas='{{datas}}' error="{{error}}" bind:onRefresh='onRefresh' bind:onLoadMore='onLoadMore'>
  <swiper
    indicator-dots="{{true}}"
    autoplay="{{true}}"
    interval="{{5000}}"
    duration="{{500}}"
  >
    <block wx:for="{{banners}}" wx:key="index">
      <swiper-item>
        <image data-imgdata="{{item}}" src="{{item.img}}" class="slide-image" mode="aspectFill" />
      </swiper-item>
    </block>
  </swiper>
  <block wx:for="{{datas}}" wx:key="index">
    <view wx:if="{{item.type === '1'}}">
      <big-img-item data="{{item}}" />
    </view>
    <view wx:if="{{item.type === '2'}}">
      <three-imgs-item data="{{item}}" />
    </view>
    <view wx:if="{{item.type === '3'}}">
      <img-item data="{{item}}" />
    </view>
    <view wx:if="{{item.type === '4'}}">
      <video-item id="{{'video_' + item.id}}" data-itemData="{{item}}" bind:bindplay="play" data="{{item}}" />
    </view>
  </block>
</list-view>

```